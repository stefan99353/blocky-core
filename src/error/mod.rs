mod cobble_error;
mod download_error;
mod installation_error;
mod launch_error;

pub use cobble_error::*;
pub use download_error::*;
pub use installation_error::*;
pub use launch_error::*;

#[cfg_attr(doc_cfg, doc(cfg(feature = "auth")))]
#[cfg(feature = "auth")]
mod auth_error;

#[cfg_attr(doc_cfg, doc(cfg(feature = "auth")))]
#[cfg(feature = "auth")]
pub use auth_error::*;
