/// Result with [`DownloadError`](DownloadError) as the error type.
pub type DownloadResult<T> = Result<T, DownloadError>;

/// An error that occurs during downloading of resources.
#[derive(Debug, thiserror::Error)]
pub enum DownloadError {
    /// Error while performing a web request.
    #[error("{0}")]
    Request(reqwest::Error),
    /// The request response
    #[error("The request response could not provide a content-length")]
    NoContentLength,
    /// IO error from interacting with files
    #[error("{0}")]
    Io(std::io::Error),
    /// The checksum of the downloaded file does not match the provided one.
    #[error("The Checksums do not match")]
    ChecksumMismatch,
}

impl From<reqwest::Error> for DownloadError {
    fn from(err: reqwest::Error) -> Self {
        Self::Request(err)
    }
}

impl From<std::io::Error> for DownloadError {
    fn from(err: std::io::Error) -> Self {
        Self::Io(err)
    }
}
