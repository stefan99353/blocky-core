use crate::error::CobbleResult;
use std::path::PathBuf;
use tokio::fs::remove_file;

/// Represents a single shaderpack.
#[cfg_attr(doc_cfg, doc(cfg(feature = "shaderpacks")))]
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Clone, Debug)]
pub struct Shaderpack {
    /// Filename
    pub name: String,
    /// Filepath
    pub path: PathBuf,
}

impl Shaderpack {
    /// Removes the shaderpack from disk.
    ///
    /// **Warning**: This will permanently delete the file!
    #[instrument(
        name = "remove_shaderpack",
        level = "trace",
        skip_all,
        fields(
            name,
            path = %self.path.to_string_lossy(),
        )
    )]
    pub async fn remove(self) -> CobbleResult<()> {
        remove_file(&self.path).await?;
        Ok(())
    }
}
