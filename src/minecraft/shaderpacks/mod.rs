mod shaderpack;

use crate::error::{CobbleError, CobbleResult};
use futures::TryStreamExt;
pub use shaderpack::*;
use std::path::{Path, PathBuf};
use tokio::fs::read_dir;
use tokio_stream::wrappers::ReadDirStream;

/// Loads all shaderpacks from the minecraft folder.
#[cfg_attr(doc_cfg, doc(cfg(feature = "shaderpacks")))]
#[instrument(
    name = "load_shaderpacks",
    level = "debug",
    skip_all,
    fields(minecraft_path)
)]
pub async fn load_shaderpacks(minecraft_path: impl AsRef<Path>) -> CobbleResult<Vec<Shaderpack>> {
    let mut shaderpacks_path = PathBuf::from(minecraft_path.as_ref());
    shaderpacks_path.push("shaderpacks");

    if !shaderpacks_path.is_dir() {
        trace!("Shaderpacks directory is empty");
        return Ok(vec![]);
    }

    trace!("Loading loader mod files...");
    let file_stream = ReadDirStream::new(read_dir(shaderpacks_path).await?);
    let shaderpacks = file_stream
        .map_err(CobbleError::from)
        .try_filter_map(|e| parse_shaderpack(e.path()))
        .try_collect()
        .await?;

    Ok(shaderpacks)
}

#[instrument(name = "parse_shaderpack", level = "trace", skip_all, fields(path,))]
pub(crate) async fn parse_shaderpack(path: impl AsRef<Path>) -> CobbleResult<Option<Shaderpack>> {
    // Check if file
    if !path.as_ref().is_file() {
        trace!("Entry is not a file.");
        return Ok(None);
    }

    // Check if archive
    let mime = match mime_guess::from_path(&path).first() {
        Some(mime) => mime,
        None => {
            trace!("Could not get MIME type for file.");
            return Ok(None);
        }
    };
    if mime != "application/zip" {
        trace!("Entry is not an archive");
        return Ok(None);
    }

    // Parse
    let name = match path.as_ref().file_name() {
        Some(name) => name.to_string_lossy().to_string(),
        None => return Ok(None),
    };

    Ok(Some(Shaderpack {
        name,
        path: PathBuf::from(path.as_ref()),
    }))
}
