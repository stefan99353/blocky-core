use crate::consts;
use crate::minecraft::fabric::FabricLibrary;
use crate::minecraft::models::{LaunchArguments, VersionType};
use serde::{Deserialize, Serialize};

/// Extension to the minecraft VersionData.
#[cfg_attr(doc_cfg, doc(cfg(feature = "fabric")))]
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FabricVersionData {
    /// Special arguments for launching
    pub arguments: Option<LaunchArguments>,
    /// Fabric version ID
    pub id: String,
    /// Inherited Minecraft version ID
    #[serde(alias = "inheritsFrom")]
    pub inherits_from: String,
    /// Libraries.
    pub libraries: Vec<FabricLibrary>,
    /// Main class / entry point
    #[serde(alias = "mainClass")]
    pub main_class: String,
    /// Type of the version
    #[serde(rename = "type")]
    pub _type: VersionType,
}

impl FabricVersionData {
    /// Gets the fabric version data from fabric servers.
    pub async fn fetch(minecraft_version: &str, loader_version: &str) -> reqwest::Result<Self> {
        let url = format!(
            "{}/versions/loader/{}/{}/profile/json",
            consts::FABRIC_BASE_V2_URL,
            minecraft_version,
            loader_version
        );
        reqwest::get(url).await?.error_for_status()?.json().await
    }
}
