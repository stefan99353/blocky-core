use std::path::Path;

use crate::minecraft::models::VersionData;
use crate::minecraft::utils::client_jar_path;
use crate::utils::Platform;

pub fn build_classpath(
    version_data: &VersionData,
    minecraft_path: impl AsRef<Path>,
    libraries_path: impl AsRef<Path>,
) -> String {
    let mut classes = version_data
        .needed_libraries()
        .into_iter()
        .map(|l| l.jar_path(&libraries_path))
        .map(|p| p.to_string_lossy().to_string())
        .collect::<Vec<_>>();

    let client_path = client_jar_path(version_data, &minecraft_path);
    classes.push(client_path.to_string_lossy().to_string());

    classes.join(&Platform::current().classpath_seperator().to_string())
}
