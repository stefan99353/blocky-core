use crate::minecraft::launch::ArgumentReplacements;
use crate::minecraft::models::VersionData;
use crate::minecraft::LaunchOptions;

pub fn build_game_args(
    version_data: &VersionData,
    argument_replacements: &ArgumentReplacements,
    launch_options: &LaunchOptions,
) -> Vec<String> {
    let mut arguments = vec![];

    // Newer versions
    if let Some(args) = &version_data.arguments {
        for argument in args.game_arguments() {
            arguments.push(argument_replacements.replace(argument.clone()));
        }
    }

    // Older versions
    if let Some(args) = &version_data.minecraft_arguments {
        for argument in args.split_whitespace() {
            arguments.push(argument_replacements.replace(argument.to_string()));
        }
    }

    // Window size
    if launch_options.fullscreen {
        arguments.push(String::from("--fullscreen"));
    } else if launch_options.enable_custom_window_size {
        arguments.extend_from_slice(&[
            String::from("--width"),
            launch_options.custom_width.to_string(),
            String::from("--height"),
            launch_options.custom_height.to_string(),
        ]);
    }

    arguments
}

pub fn build_jvm_args(
    version_data: &VersionData,
    argument_replacements: &ArgumentReplacements,
    launch_options: &LaunchOptions,
) -> Vec<String> {
    let mut arguments = vec![];

    // Memory
    if launch_options.enable_custom_memory {
        arguments.push(format!("-Xms{}M", launch_options.custom_min_memory));
        arguments.push(format!("-Xmx{}M", launch_options.custom_max_memory));
    }

    // JVM arguments
    if let Some(jvm_arguments) = &launch_options.custom_jvm_arguments {
        // Custom
        for argument in jvm_arguments.split_whitespace() {
            arguments.push(argument_replacements.replace(argument.to_string()));
        }
    } else if let Some(args) = &version_data.arguments {
        // Version Data
        for argument in args.jvm_arguments() {
            arguments.push(argument_replacements.replace(argument.to_string()));
        }
    }

    // Natives path
    if !arguments
        .iter()
        .any(|arg| arg.starts_with("-Djava.library.path="))
    {
        let arg = String::from("-Djava.library.path=${natives_directory}");
        arguments.push(argument_replacements.replace(arg));
    }

    // Classpath
    if !arguments.iter().any(|arg| arg.starts_with("-cp")) {
        let argument = String::from("${classpath}");
        arguments.push("-cp".to_string());
        arguments.push(argument_replacements.replace(argument));
    }

    arguments
}
