use crate::minecraft::launch::ArgumentReplacements;
use crate::minecraft::launch::{
    build_classpath, build_game_args, build_jvm_args, build_logging_arg,
};
use crate::minecraft::models::VersionData;
use crate::minecraft::LaunchOptions;
use std::path::Path;
use std::process::Command;

/// Builds the launch command for Minecraft.
/// The game needs to be installed to launch.
#[instrument(
    name = "build_launch_command",
    level = "trace",
    skip_all,
    fields(
        version = version_data.id,
        launch_options,
        minecraft_path,
        libraries_path,
        assets_path,
        natives_path,
        log_configs_path,
    )
)]
pub fn build_launch_command(
    version_data: &VersionData,
    launch_options: &LaunchOptions,
    minecraft_path: impl AsRef<Path>,
    libraries_path: impl AsRef<Path>,
    assets_path: impl AsRef<Path>,
    natives_path: impl AsRef<Path>,
    log_configs_path: impl AsRef<Path>,
) -> Command {
    // Classpath
    trace!("Build classpath");
    let classpath = build_classpath(version_data, &minecraft_path, &libraries_path);

    // Argument replacements
    trace!("Create argument replacements");
    let argument_replacements = ArgumentReplacements::build(
        launch_options,
        version_data,
        classpath,
        minecraft_path.as_ref().to_string_lossy().to_string(),
        assets_path.as_ref().to_string_lossy().to_string(),
        natives_path.as_ref().to_string_lossy().to_string(),
    );

    // Arguments
    trace!("Build JVM arguments");
    let jvm_args = build_jvm_args(version_data, &argument_replacements, launch_options);
    trace!("Build game arguments");
    let game_args = build_game_args(version_data, &argument_replacements, launch_options);

    // Logging config
    trace!("Build log argument");
    let log_arg = build_logging_arg(version_data, &log_configs_path);

    trace!("Build command");
    combine_launch_parts(
        launch_options.java_executable.clone(),
        minecraft_path,
        jvm_args,
        game_args,
        log_arg,
        version_data.main_class.clone(),
        launch_options.environment_variables.clone(),
    )
}

pub(crate) fn combine_launch_parts(
    java_exec: String,
    minecraft_path: impl AsRef<Path>,
    jvm_args: Vec<String>,
    game_args: Vec<String>,
    log_argument: Option<String>,
    main_class: String,
    env_vars: Vec<(String, Option<String>)>,
) -> Command {
    let mut command = Command::new(java_exec);

    command.current_dir(minecraft_path);
    command.args(jvm_args);

    if let Some(log_arg) = log_argument {
        command.arg(log_arg);
    }

    command.arg(main_class);
    command.args(game_args);

    command.envs(
        env_vars
            .into_iter()
            .map(|(k, v)| (k, v.unwrap_or_default())),
    );

    command
}
