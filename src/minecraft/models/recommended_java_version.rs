use serde::{Deserialize, Serialize};

/// Recommended java version.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RecommendedJavaVersion {
    /// Component
    pub component: String,
    /// Major Java version
    #[serde(alias = "majorVersion")]
    pub major_version: i32,
}
