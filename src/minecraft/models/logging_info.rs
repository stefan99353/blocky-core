use crate::minecraft::models::file::File;
use serde::{Deserialize, Serialize};

/// Information about the logging configuration
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LoggingInfo {
    /// Logging in the client
    pub client: ClientLoggingInfo,
}

/// Client logging information
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ClientLoggingInfo {
    /// The argument passed for logging.
    pub argument: String,
    /// The logging configuration file.
    pub file: File,
    /// Type of the logging file
    #[serde(rename = "type")]
    pub _type: String,
}
