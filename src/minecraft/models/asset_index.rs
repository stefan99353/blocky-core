use crate::consts;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::path::{Path, PathBuf};

/// Map of all the assets for a Minecraft version.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AssetIndex {
    /// Wether to install the assets as resources or not.
    ///
    /// Used primarily on older versions (pre1.6).
    #[serde(default)]
    pub map_to_resources: bool,
    /// Asset objects.
    ///
    /// Key is the name of the file.
    pub objects: HashMap<String, AssetInfo>,
}

/// Information of a single asset.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AssetInfo {
    /// SHA1 of the asset file.
    pub hash: String,
    /// Size of the asset file.
    pub size: usize,
}

impl AssetInfo {
    /// Builds the complete path for the asset file.
    pub fn asset_path(&self, assets_path: impl AsRef<Path>) -> PathBuf {
        let mut asset_path = PathBuf::from(assets_path.as_ref());

        asset_path.push(self.relative_asset_path());

        asset_path
    }

    /// Builds the path for the asset file relative to the assets folder.
    pub fn relative_asset_path(&self) -> PathBuf {
        let mut asset_path = PathBuf::new();

        asset_path.push("objects");
        asset_path.push(self.hash.chars().take(2).collect::<String>().as_str());
        asset_path.push(&self.hash);

        asset_path
    }

    /// Builds the complete path for the asset file mapped as a resource.
    pub fn resource_path(key: &str, minecraft_path: impl AsRef<Path>) -> PathBuf {
        let mut resource_path = PathBuf::from(minecraft_path.as_ref());

        resource_path.push("resources");
        resource_path.push(key);

        resource_path
    }

    /// Builds the download URL for the asset.
    pub fn download_url(&self) -> String {
        let mut url = vec![consts::MC_ASSETS_BASE_URL];

        let part = self.hash.chars().take(2).collect::<String>();

        url.push(part.as_str());
        url.push(&self.hash);

        url.join("/")
    }
}
