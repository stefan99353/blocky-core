use crate::consts;
use crate::minecraft::models::VersionSummary;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

/// Manifest with all Minecraft versions.
/// Includes identifiers for the latest release and snapshot version.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct VersionManifest {
    /// Latest release and snapshot version
    pub latest: LatestVersion,
    /// Map of all versions
    pub versions: HashMap<String, VersionSummary>,
}

impl VersionManifest {
    /// Gets the manifest from Minecraft servers.
    pub async fn fetch() -> reqwest::Result<Self> {
        reqwest::get(consts::MC_VERSION_MANIFEST_URL)
            .await?
            .error_for_status()?
            .json::<VersionManifestResponse>()
            .await
            .map(Self::from)
    }
}

/// Holds latest release and snapshot version IDs.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LatestVersion {
    /// Latest stable release
    pub release: String,
    /// Latest snapshot
    pub snapshot: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct VersionManifestResponse {
    pub latest: LatestVersion,
    pub versions: Vec<VersionSummary>,
}

impl From<VersionManifestResponse> for VersionManifest {
    fn from(manifest: VersionManifestResponse) -> Self {
        let mut versions = HashMap::new();

        for version in manifest.versions {
            versions.insert(version.id.clone(), version);
        }

        Self {
            latest: manifest.latest,
            versions,
        }
    }
}
