use crate::minecraft::models::{AssetIndex, VersionData};
use derive_builder::Builder;
use std::path::PathBuf;

/// Options for installing Minecraft.
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Builder, Clone, Debug)]
pub struct InstallOptions {
    /// Minecraft version data
    pub version_data: VersionData,
    /// Minecraft asset index
    pub asset_index: AssetIndex,
    /// Path for libraries
    pub libraries_path: PathBuf,
    /// Path for native libraries
    pub natives_path: PathBuf,
    /// Path for assets
    pub assets_path: PathBuf,
    /// Path for log configs
    pub log_configs_path: PathBuf,
    /// Path for minecraft
    pub minecraft_path: PathBuf,
    /// Number of parallel downloads
    #[builder(default = "4")]
    pub parallel_downloads: u16,
    /// Number of download retries
    #[builder(default = "1")]
    pub download_retries: u16,
    /// Whether to verify downloads
    #[builder(default = "true")]
    pub verify_downloads: bool,
}
