use tokio::sync::mpsc::{channel, Receiver, Sender};

use crate::minecraft::install::{
    AssetInstallationUpdate, ClientInstallationUpdate, LibraryInstallationUpdate,
    LogConfigInstallationUpdate,
};

/// Installation Update that is sent during installation.
#[derive(Clone, Debug)]
pub enum InstallationUpdate {
    /// Progress Update for installing a library.
    /// Contains the name of the library and the progress.
    Library((String, LibraryInstallationUpdate)),
    /// Progress Update for installing an asset.
    /// Contains the name of the asset and the progress.
    Asset((String, AssetInstallationUpdate)),
    /// Progress Update for installing the log config.
    /// Contains the progress.
    LogConfig(LogConfigInstallationUpdate),
    /// Progress Update for installing the client.
    /// Contains the progress.
    Client(ClientInstallationUpdate),
}

impl InstallationUpdate {
    /// Helper function for getting the update
    pub fn channel(buffer: usize) -> (Sender<Self>, Receiver<Self>) {
        channel(buffer)
    }
}
