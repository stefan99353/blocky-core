use crate::error::CobbleResult;
use std::path::PathBuf;
use time::OffsetDateTime;
use tokio::fs::remove_file;

/// Represents a single screenshot.
#[cfg_attr(doc_cfg, doc(cfg(feature = "screenshots")))]
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
#[derive(Clone, Debug)]
pub struct Screenshot {
    /// Name of the save game
    pub name: String,
    /// Path to the save game
    pub path: PathBuf,
    /// Created timestamp
    #[cfg_attr(feature = "serde", serde(with = "time::serde::rfc3339::option"))]
    pub created: Option<OffsetDateTime>,
}

impl Screenshot {
    /// Removes the screenshot from disk.
    ///
    /// **Warning**: This will permanently delete the file!
    #[instrument(
        name = "remove_screenshot",
        level = "trace",
        skip_all,
        fields(
            name,
            path = %self.path.to_string_lossy(),
        )
    )]
    pub async fn remove(self) -> CobbleResult<()> {
        remove_file(&self.path).await?;
        Ok(())
    }
}
