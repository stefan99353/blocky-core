use crate::error::CobbleResult;
use crate::instance::Instance;
use crate::minecraft::models::VersionData;
use tokio::fs::{create_dir_all, File};
use tokio::io::{AsyncReadExt, AsyncWriteExt};

impl Instance {
    pub(crate) async fn save_version_data(&self, version_data: &VersionData) -> CobbleResult<()> {
        let version_data_json = serde_json::to_vec_pretty(&version_data)?;

        if let Some(parent) = self.version_data_path().parent() {
            trace!("Creating parent folder for version data JSON");
            create_dir_all(parent).await?;
        }

        let mut file = File::create(self.version_data_path()).await?;
        file.write_all(&version_data_json).await?;
        file.sync_all().await?;

        Ok(())
    }

    pub(crate) async fn read_version_data(&self) -> CobbleResult<VersionData> {
        let mut file = File::open(self.version_data_path()).await?;
        let mut buffer = vec![];
        file.read_to_end(&mut buffer).await?;
        let version_data = serde_json::from_slice(&buffer)?;

        Ok(version_data)
    }
}
