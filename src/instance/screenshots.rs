use crate::error::CobbleResult;
use crate::minecraft::{load_screenshots, Screenshot};
use crate::Instance;
use std::path::PathBuf;

impl Instance {
    /// Path to the .minecraft/screenshots folder.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "screenshots")))]
    pub fn screenshots_path(&self) -> PathBuf {
        let mut screenshots_path = self.dot_minecraft_path();
        screenshots_path.push("screenshots");
        screenshots_path
    }

    /// Loads all screenshots from this instance.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "screenshots")))]
    pub async fn load_screenshots(&self) -> CobbleResult<Vec<Screenshot>> {
        load_screenshots(self.dot_minecraft_path()).await
    }
}
