use crate::error::CobbleResult;
use crate::minecraft::{load_loader_mods, parse_loader_mod, LoaderMod};
use crate::Instance;
use std::path::{Path, PathBuf};
use tokio::fs::create_dir_all;

impl Instance {
    /// Path to the .minecraft/mods folder.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "loader-mods")))]
    pub fn loader_mods_path(&self) -> PathBuf {
        let mut loader_mods_path = self.dot_minecraft_path();
        loader_mods_path.push("mods");
        loader_mods_path
    }

    /// Loads all log files from this instance.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "loader-mods")))]
    pub async fn load_loader_mods(&self) -> CobbleResult<Vec<LoaderMod>> {
        load_loader_mods(self.loader_mods_path()).await
    }

    /// Adds a loader mod.
    ///
    /// Returns `None` when `src` is not a valid resourcepack.
    #[instrument(name = "add_loader_mod", level = "trace", skip_all, fields(src))]
    #[cfg_attr(doc_cfg, doc(cfg(feature = "loader-mods")))]
    pub async fn add_loader_mod(&self, src: impl AsRef<Path>) -> CobbleResult<Option<LoaderMod>> {
        trace!("Validating src...");
        if parse_loader_mod(PathBuf::from(src.as_ref()))
            .await?
            .is_none()
        {
            return Ok(None);
        }

        trace!("Building new path for loader_mod");
        let file_name = src
            .as_ref()
            .file_name()
            .ok_or_else(|| std::io::Error::new(std::io::ErrorKind::Other, "Path ends with '..'"))?;
        let mut loader_mod_path = self.loader_mods_path();
        loader_mod_path.push(file_name);
        tracing::Span::current().record("dest", loader_mod_path.to_string_lossy().to_string());

        if let Some(parent) = loader_mod_path.parent() {
            trace!("Creating mods folder...");
            create_dir_all(parent).await?;
        }

        trace!("Copying loader mod...");
        tokio::fs::copy(src, &loader_mod_path).await?;

        trace!("Parsing new loader mod...");
        let loader_mod = parse_loader_mod(loader_mod_path).await?;
        Ok(loader_mod)
    }
}
