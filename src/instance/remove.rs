use tokio::fs::remove_dir_all;

use crate::error::CobbleResult;
use crate::Instance;

impl Instance {
    /// Removes the instance from disk.
    ///
    /// **Warning**: This will permanently delete the following files:
    ///
    /// - Resourcepacks
    /// - Save games
    /// - Screenshots
    /// - Servers
    /// - Shaderpacks
    /// - Mods
    ///
    /// It basically deletes the whole .minecraft folder.
    #[instrument(name = "remove_instance", level = "trace", skip_all, fields(name,))]
    pub async fn remove(self) -> CobbleResult<()> {
        remove_dir_all(self.instance_path()).await?;
        Ok(())
    }
}
