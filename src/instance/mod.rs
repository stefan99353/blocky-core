#[cfg(feature = "backup")]
mod backup;
#[cfg(not(feature = "fabric"))]
mod install;
#[cfg(not(feature = "fabric"))]
mod launch;
mod paths;
mod remove;
mod version_data;

// Vanilla features
#[cfg(feature = "log-files")]
mod log_files;
#[cfg(feature = "resourcepacks")]
mod resourcepacks;
#[cfg(feature = "save-games")]
mod save_games;
#[cfg(feature = "screenshots")]
mod screenshots;
#[cfg(feature = "servers")]
mod servers;

// Modded features
#[cfg(feature = "fabric")]
mod fabric;
#[cfg(feature = "loader-mods")]
mod loader_mods;
#[cfg(feature = "shaderpacks")]
mod shaderpacks;

use derive_builder::Builder;
use std::path::PathBuf;
use time::OffsetDateTime;
use uuid::Uuid;

/// A Minecraft instance with defined locations for installing libraries, assets and other game data.
/// An instance enables multiple instances to be installed while sharing common data like assets and libraries.
///
/// An instance can be created using the builder pattern:
///
/// ```rust
/// # use cobble_core::instance::{Instance, InstanceBuilder, InstanceBuilderError};
/// # fn function() -> Result<(), InstanceBuilderError> {
///
/// let instance: Instance = InstanceBuilder::default()
///     .name("Minecraft Instance".to_string())
///     .version("1.18.2".to_string())
///     .instance_path(".".to_string())
///     .libraries_path("./libraries".to_string())
///     .assets_path("./assets".to_string())
///     .build()?;
///
/// # Ok(())
/// # }
/// ```
#[derive(serde::Deserialize, serde::Serialize, Builder, Clone, Debug)]
pub struct Instance {
    /// Instance [`UUID`](uuid::Uuid).
    ///
    /// Defaults to `Uuid::new_v4()`.
    #[builder(default = "Uuid::new_v4()")]
    pub uuid: Uuid,

    /// Name of the instance.
    #[builder(setter(into))]
    pub name: String,

    /// Optional description of the instance.
    #[builder(setter(into, strip_option), default)]
    pub description: Option<String>,

    /// Minecraft version identifier for this instance.
    #[builder(setter(into))]
    pub version: String,

    /// Path for the instance folder.
    /// This path contains the `.minecraft`folder and some other metadata.
    #[builder(setter(into))]
    pub instance_path: PathBuf,

    /// Path for the libraries.
    #[builder(setter(into))]
    pub libraries_path: PathBuf,

    /// Path for the assets.
    #[builder(setter(into))]
    pub assets_path: PathBuf,

    /// Enables fullscreen.
    /// Overwrites the custom window size.
    ///
    /// Defaults to `false`.
    #[builder(default)]
    pub fullscreen: bool,

    /// Enables custom window size.
    /// Takes effect when not launching in fullscreen.
    /// The width and height can be configured with `custom_width` and `custom_height`.
    ///
    /// Defaults to `false`.
    #[builder(default)]
    pub enable_custom_window_size: bool,

    /// Custom game window width.
    /// Used when `enable_custom_window_size` is enabled.
    ///
    /// Defaults to `1280`.
    #[builder(default = "1280")]
    pub custom_width: u32,

    /// Custom game window height.
    /// Used when `enable_custom_window_size` is enabled.
    ///
    /// Defaults to `720`.
    #[builder(default = "720")]
    pub custom_height: u32,

    /// Enables custom JVM memory restrictions.
    /// The minimum and maximum can be configured with `custom_min_memory` and `custom_max_memory`.
    ///
    /// Defaults to `false`.
    #[builder(default)]
    pub enable_custom_memory: bool,

    /// JVM initial heap size in megabytes.
    /// Adds the `-Xms` option to the command.
    /// Gets added before `custom_jvm_args`.
    /// Used when `enable_custom_memory` is enabled.
    ///
    /// Defaults to `1024`.
    #[builder(default = "1024")]
    pub custom_min_memory: u32,

    /// JVM max heap size in megabytes.
    /// Adds the `-Xmx` opti`on to the command.
    /// Gets added before `custom_jvm_args`.
    /// Used when `enable_custom_memory` is enabled.
    ///
    /// Defaults to `2048`.
    #[builder(default = "2048")]
    pub custom_max_memory: u32,

    /// Custom java executable.
    ///
    /// Defaults to `None`.
    #[builder(setter(into, strip_option), default)]
    pub custom_java_executable: Option<String>,

    /// Custom JVM arguments
    ///
    /// Defaults to `None`.
    #[builder(setter(into, strip_option), default)]
    pub custom_jvm_arguments: Option<String>,

    /// Environment variables used for launching the game.
    ///
    /// Defaults to `vec![]`.
    #[builder(default)]
    pub environment_variables: Vec<(String, Option<String>)>,

    /// Flag whether the instance was installed.
    ///
    /// Defaults to `false`.
    #[builder(default)]
    pub installed: bool,

    /// Created timestamp.
    ///
    /// Defaults to `OffsetDateTime::now_utc()`.
    #[builder(default = "OffsetDateTime::now_utc()")]
    #[cfg_attr(feature = "serde", serde(with = "time::serde::rfc3339"))]
    pub created: OffsetDateTime,

    /// Fabric loader version that is used by this instance.
    ///
    /// Defaults to `None`.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "fabric")))]
    #[cfg(feature = "fabric")]
    #[builder(default)]
    #[cfg_attr(feature = "serde", serde(default))]
    pub fabric_version: Option<String>,
}
