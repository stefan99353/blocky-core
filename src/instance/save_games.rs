use crate::error::CobbleResult;
use crate::minecraft::{load_save_games, SaveGame};
use crate::Instance;
use std::path::PathBuf;

impl Instance {
    /// Path to the .minecraft/saves folder.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "save-games")))]
    pub fn save_games_path(&self) -> PathBuf {
        let mut saves_path = self.dot_minecraft_path();
        saves_path.push("saves");
        saves_path
    }

    /// Loads all save games from this instance.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "save-games")))]
    pub async fn load_save_games(&self) -> CobbleResult<Vec<SaveGame>> {
        load_save_games(self.save_games_path()).await
    }

    /// Imports a save game from an exported archive.
    /// Parses the imported save game and returns `None` if parsing fails.
    #[cfg_attr(doc_cfg, doc(cfg(feature = "backup")))]
    #[cfg(feature = "backup")]
    pub async fn import_save_game(
        &self,
        src: impl AsRef<std::path::Path>,
    ) -> CobbleResult<Option<SaveGame>> {
        let save_games_path = self.save_games_path();

        if !save_games_path.is_dir() {
            tokio::fs::create_dir_all(&save_games_path).await?;
        }

        SaveGame::import(save_games_path, src).await
    }
}
