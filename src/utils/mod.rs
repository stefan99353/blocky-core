mod download;
mod either;
mod os;

pub use download::DownloadProgress;
pub(crate) use download::{download, download_progress_channel, Download};
pub(crate) use either::Either;
pub(crate) use os::{Architecture, Platform};

#[cfg(feature = "internal-archive")]
mod archive;

#[cfg(feature = "internal-archive")]
pub(crate) use archive::*;
