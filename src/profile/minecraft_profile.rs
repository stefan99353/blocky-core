use crate::consts;
use crate::error::AuthResult;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct MinecraftProfile {
    pub id: String,
    pub name: String,
    // pub capes: Vec<serde_json::Value>,
    // pub skins: Vec<serde_json::Value>,
}

#[instrument(name = "get_profile", level = "trace", skip_all)]
pub async fn get_profile(minecraft_token: &str) -> AuthResult<MinecraftProfile> {
    let client = reqwest::Client::new();
    let response = client
        .get(consts::MC_PROFILE_URL)
        .bearer_auth(minecraft_token)
        .send()
        .await?
        .error_for_status()?
        .json::<MinecraftProfile>()
        .await?;

    Ok(response)
}
