use crate::consts;
use crate::error::AuthResult;
use serde::Deserialize;

#[derive(Clone, Debug)]
pub struct Entitlements {
    pub entitlements: Vec<Entitlement>,
    pub signature: String,
    pub key_id: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Entitlement {
    pub name: String,
    pub signature: String,
}

#[instrument(name = "get_entitlements", level = "trace", skip_all)]
pub async fn get_entitlements(minecraft_token: &str) -> AuthResult<Entitlements> {
    let client = reqwest::Client::new();
    let response = client
        .get(consts::MC_ENTITLEMENTS_URL)
        .bearer_auth(minecraft_token)
        .send()
        .await?
        .error_for_status()?
        .json::<EntitlementsResponse>()
        .await?;

    Ok(response.into())
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
struct EntitlementsResponse {
    items: Vec<Entitlement>,
    signature: String,
    key_id: String,
}

impl From<EntitlementsResponse> for Entitlements {
    fn from(resp: EntitlementsResponse) -> Self {
        Self {
            entitlements: resp.items,
            signature: resp.signature,
            key_id: resp.key_id,
        }
    }
}
