# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Added

- Instance method for screenshots path `screenshots_path`

### Changed

- Updated dependencies

### Security

- Increased minimum version of `tar` to `>=0.4.36` to patch [RUSTSEC-2021-0080](https://rustsec.org/advisories/RUSTSEC-2021-0080.html)